package tema2;

import java.util.ArrayList;
import java.util.Random;
import java.io.*;


public class SimulationManager  extends FileNotFoundException implements Runnable {
	
	
	public int timeLimit;
	public int maxProccesingTime;
	public int minProccesingTime;
	public int maxArrivalTime;
	public int minArrivalTime;
	public int numberOfServers;
	public int numberOfClients;
	public float averageTime;
	
	private Scheduler scheduler;
	private ArrayList<Task> GeneratedTasks;
	
	static int var;
	static int verif;
	
	
	int okk=0;
	
	String citeste;
	FileOutputStream g=null;{
	 try {
	 		g=new FileOutputStream("out-test-1.txt");
	 	} catch (FileNotFoundException e) {
	 		// TODO Auto-generated catch block
	 		e.printStackTrace();
	 	}
	}
	
	 {FileInputStream f = null;
	try {
		f = new FileInputStream("in-test-1.txt");
	} catch (FileNotFoundException e2) {
		// TODO Auto-generated catch block
		e2.printStackTrace();
	}
	InputStreamReader fchar=new InputStreamReader(f);
	BufferedReader buf=new BufferedReader(fchar);
	
	{try {
		citeste=buf.readLine();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	numberOfClients=Integer.parseInt(citeste);
	try {
		citeste=buf.readLine();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	numberOfServers=Integer.parseInt(citeste);
	try {
		citeste=buf.readLine();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	timeLimit=Integer.parseInt(citeste);
	try {
		citeste=buf.readLine();
	} catch (IOException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	String[] arrivalTime = citeste.split(",");
	
	minArrivalTime=Integer.parseInt(arrivalTime[0]);
	maxArrivalTime=Integer.parseInt(arrivalTime[1]);
	 
	 try {
		citeste=buf.readLine();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	 String[] proccesingTime= citeste.split(",");

		
	 minProccesingTime=Integer.parseInt(proccesingTime[0]);
	 maxProccesingTime=Integer.parseInt(proccesingTime[1]);}}
	
	
	
	public SimulationManager()
	{  
	  scheduler=new Scheduler(numberOfServers);
	  GeneratedTasks=generateNRandomTasks();	
	}
	

	
	private ArrayList<Task> generateNRandomTasks()
	{   
		
   ArrayList<Task> random=new ArrayList<Task>();
		int N=numberOfClients;
		 while(N>0)
		 {	
		Task t=new Task();
		Random r=new Random();
		t.setID((int)(1+var)%(numberOfClients+var));
		if(t.getID()==0)
			t.setID(t.getID()+(var+1));
	      int x= (int) ((Math.random() * ((maxArrivalTime - minArrivalTime) + 1)) + minArrivalTime);
	      while(x<verif)
	    	  x= (int) ((Math.random() * ((maxArrivalTime - minArrivalTime) + 1)) + minArrivalTime);
	      t.setArrivalTime(x);
	     
		t.setProccesingTime((int) ((Math.random() * ((maxProccesingTime - minProccesingTime) + 1)) + minProccesingTime));
		var=var+1;
		verif=t.getArrivalTime();
		N--;
		random.add(t);
		}
		 return random;
	}
	  
	public float average=0;
    public float av()
    {
    	for(int i=0;i<this.GeneratedTasks.size();i++)
    	{
    		this.average=this.average+this.GeneratedTasks.get(i).getProccesingTime();
    	}
    	this.average=this.average/GeneratedTasks.size();
    	return this.average;
    }
    
    
	
	
	
	public void run()
	{int CurrentTime=0;
		average=av();
		while(CurrentTime<=timeLimit)
		{   // System.out.println(timeLimit);
			for(int i=0;i<numberOfServers;i++)
			{		scheduler.getServer().get(numberOfServers-i-1).run();
			       try {
					scheduler.getServer().get(i).sleep(10);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		    }
			
			int index=0;
			
			if(index<GeneratedTasks.size())
			{
				int availability=0;
				int bagat=0;
				Task aux=new Task();
				
				if(index<GeneratedTasks.size()&&GeneratedTasks.get(index).getArrivalTime()<=CurrentTime&&availability==0)
				{
					 aux=GeneratedTasks.get(index);
					bagat=index;
					GeneratedTasks.remove(index);
					for(int i=0;i<numberOfServers&&bagat==index;i++)
					{
						scheduler.dispatchTask(aux);
						
						index++;
					
					if(i==(numberOfServers-1))
					{
						availability=1;
					}
					}
					
							
					
				}
				
				
			     if(GeneratedTasks.size()==0&&scheduler.isEmtpy()==true&&okk==0)
			     {  averageTime=CurrentTime;
			     okk=1;
			     }
				   
			
			   }
		    
			if(GeneratedTasks.size()==0&&scheduler.isEmtpy()==true&&okk==0)
		     {  averageTime=CurrentTime;
		     okk=1;
		     }
		 	
		 	PrintStream gchar=new PrintStream(g);
		     try {
				afisare(gchar,GeneratedTasks,scheduler,CurrentTime,averageTime,timeLimit,average);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		     
		     CurrentTime++;
		}
			
	
		}
	
			
	
	public static void main(String[] args) throws FileNotFoundException {
		// TODO Auto-generated method stub
		SimulationManager gen=new SimulationManager();
		
		FileOutputStream g=new FileOutputStream(args[1]);
	    FileInputStream f =new FileInputStream(args[0]);
		
		Thread t=new Thread(gen);
		t.start();


	}
public static void afisare(PrintStream gchar, ArrayList<Task> GeneratedTask, Scheduler scheduler,int CurrentTime,float averageTime,int timeLimit,float average) throws InterruptedException {
	   
	if(CurrentTime<=timeLimit)
    {gchar.print("TIME LIMIT:");     
	gchar.print(CurrentTime);
	gchar.println();
    gchar.print("Waiting clients: ");
	 	for(int i =0; i < GeneratedTask.size(); i++)
	 		
	 		gchar.print(GeneratedTask.get(GeneratedTask.size()-i-1).toString() + " ; ");
	 	gchar.println();
	 	for(int i = 0; i <scheduler.getServer().size(); i++) 
	 		{//System.out.println();
	 		gchar.println(scheduler.getServer().get(i).toString());
	 		}
	 	gchar.println();
    }	
	 	if(CurrentTime==timeLimit)
	 	{	 //	if(!(averageTime==0||averageTime==-1))
	 	{gchar.print("AVERAGE TIME:");
	 	//gchar.println(timeLimit/(averageTime-1));
	 	//gchar.println();
	 	gchar.println(average);
	 	}
	 	}
	// afisare(), GeneratedTasks, scheduler); 
    
	
	

}
	

}
