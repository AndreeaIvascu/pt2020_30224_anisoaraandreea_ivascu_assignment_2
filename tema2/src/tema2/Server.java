package tema2;

import java.util.ArrayList;

public class Server extends Thread{
    
	private int index;
	private ArrayList<Task> tasks;//intr-un server se pot afla la un moment dat mai multe task-uri
	private int waitingPeriod; //cat timp serverul nu va fi closed
	private boolean closed;//disponibilitatea unui server: pt true->server gol
	
	
	
  public Server(int index)
  {
	  this.setIndex(index);
	  tasks=new ArrayList<Task>();
	  this.waitingPeriod=0;
	  this.closed=true;
	  
  }
	
	
	public void addTask(Task task)
	{
		tasks.add(task);
		this.waitingPeriod = waitingPeriod+task.getProccesingTime();
		this.closed=false;
       
	}
    
	
	public boolean isEmpty()
	{
		if(this.closed==true)
			return true; //server gol
		return false; //in server se afla cel putin un task
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
	if(isEmpty()!=true)
	{// int  indice=this.tasks.size()-1;
		if(this.tasks.get(0).getProccesingTime()>0)
		{
			this.tasks.get(0).setProccesingTime(this.tasks.get(0).getProccesingTime()-1);
			this.waitingPeriod=this.waitingPeriod-1;
		}
	 
		  if(this.tasks.get(0).getProccesingTime()==0)
		  {
			  
			  this.tasks.remove(0);
			  if(this.tasks.size()==0)
				  this.closed=true;
			  
			  
		  }
                 		
		
	
	}
		
	}
	
	@Override
	public String toString()
	{   
		int i=0;
		String s="";
		String t1="Queue"+index+":";
		String t="";
		if(this.closed==true)
			s="Queue"+index+":"+"closed";
		else
		{  while(i<this.tasks.size()) {
			{	t=t+this.tasks.get(i).toString();
			i++;}	
			s=t1+t;
		}
		
		}
		return s;
	}
	

	public int getIndex() {
		return index;
	}


	public void setIndex(int index) {
		this.index = index;
	}


	public int getWaitingPeriod() {
		return waitingPeriod;
	}


	public void setWaitingPeriod(int waitingPeriod) {
		this.waitingPeriod = waitingPeriod;
	}




	

}
